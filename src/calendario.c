#include <stdio.h>
#include <string.h>

#include <calendario.h>

//fución para imprimir el un mes del calendario real.
void printCalendar( int month,int year){
	int days_month;
	char months[12][9]={"January",
			    "February", 
			    "March", 
			    "April", 
			    "May", 
			    "June", 
			    "July", 
			    "August", 
			    "September", 
			    "October", 
			    "November", 
			    "December"};
	int days_monthF;
	if(year%400==0){
        	days_monthF=29;
      	}
        else{
        	if(year%4==0 && year%100!=0){
                	days_monthF=29;
               	}
               	else{
                	days_monthF=28;
                                }
       	}
	switch(month){
		case 1: case 3: case 5: case 7: case 8: case 10: case 12:
			days_month=31;
			break;
		case 4: case 6: case 9: case 11:
			days_month=30;
			break;
		case 2:
			days_month=days_monthF;
			break;	
	}
	int iMonth;
	int nDay;
	iMonth=month-1;
	char nMonth[9];
	strcpy(nMonth,months[iMonth]);
	
	/*calculo que dia es, lunes, martes, miercoles,.. 
	  el primer dia del mes.*/
	nDay=year-2000;
	nDay=nDay-1;
	int nDayS=nDay/4;
	nDay=nDay+nDayS;
	while(nDay>=7){
		nDay=nDay-7;
	}
	int x;
	switch(month){
		case 1: case 10:
			x=0;
			break;
		case 2: case 3: case 11:
			x=3;
			break;
		case 4: case 7:
			x=6;
			break;
		case 5:
			x=1;
			break;
		case 6:
			x=4;

			break;
		case 8:
			x=2;
			break;
		case 9: case 12:
			x=5;
			break;
	}
	nDay=nDay+x;
	nDay=nDay+1;
	nDay=nDay%7;
	if(days_monthF==29){
		nDay=nDay+1;
	}
	char *first_week[7]={"                                1  ",
			     "  1    2    3    4    5    6    7  ",
			     "       1    2    3    4    5    6  ",
			     "            1    2    3    4    5  ",
			     "                 1    2    3    4  ",
			     "                      1    2    3  ",
			     "                           1    2  "};

        printf("\n           %s %i\n",nMonth,year);
	printf("\n LUN  MAR  MIE  JUE  VIE  SAB  DOM \n");
	
	/* se imprime la primera semana del mes según los 
	   càlculos para obtener que dìa es el primer dìa de cada mes */
	printf("%s \n\n",first_week[nDay]);
	int count=7;
	int j=nDay;
	if(j==0){ j=7; }

	for(int i=9-j; i<=days_month; i++){
		if(count>0){
			if(i<10){
				printf("  %i  ",i);
			}
			if(i>=10){
				printf("  %i ",i);
			}
			count=count-1;
		}
		else{
			printf("\n\n");
			count=7;
			i-=1;
		}	
	}printf("\n\n");
}



