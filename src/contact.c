#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <contact.h>

#include <unistd.h>
#include <fcntl.h>

Contact* createContact(Contact *head,char *name, 
				     char *lastName, 
				     char *birthdate, 
				     char *address, 
				     char *phone){

	Contact *c=(Contact*)malloc(sizeof(Contact));	
	strcpy(c->name,name);
	strcpy(c->lastName,lastName);
	strcpy(c->birthdate,birthdate);
	strcpy(c->address,address);
	strcpy(c->phone,phone);
	c->next=NULL;

	saveContact(c);

	if(head!=NULL){
		Contact *current=head;
		while(current->next!=NULL){current=current->next;}
		current->next=c;	
	}else{
		head=c;
	}
	return head;
	
}

void showContactList(Contact *head){      	
	Contact* current=head;
	int i=1; 	
	if(head!=NULL){
		printf("Lista de contactos: \n");
		while(current!=NULL){
			printf("%d>> \n", i);
			printf("    Nombre: %s",current->name);
			printf("    Apellido: %s",current->lastName);	
			current=current->next;
			i++;
		}
	}else{
		printf("No existen contactos \n");
	}
			
}

void showContactDetails(Contact *head){
	Contact* current=head;
	int i=1; 	
	if(head!=NULL){
		printf("Lista de contactos detallada: \n");
		while(current!=NULL){
			printf("%d>> \n", i);
			printf("    Nombre: %s",current->name);
			printf("    Apellido: %s",current->lastName);	
			printf("    Cumpleaños: %s",current->birthdate);
			printf("    Direccion: %s",current->address);
			printf("    Telefono: %s",current->phone);  
			current=current->next;
			i++;
		}
	}else{
		printf("No existen contactos \n");
	}
	
}


Contact* deleteContact(Contact *head,int noContactToDelete){
	Contact *current=head;
	Contact *previous=NULL;
	int i=1;

	while (current!=NULL){
		if(i==noContactToDelete){
			if (previous==NULL){
				head = current->next;
				free(current);
				printf("El contacto fue borrado!\n");
				return head;
			}else{
				previous->next = current->next;
				free(current);
				printf("El contacto fue borrado!\n");
				return head;
			}
		}
		previous=current;
		current=current->next;
		i++;
	}	
	printf("No existe tal contacto \n");
	return head;
}

int existC(Contact* head, int noContactToModify){
	Contact *current=head;
	int i=1;
	while (current!=NULL){
		if(i==noContactToModify){ return 1; }
		current=current->next;
		i++;
	}	
	return 0;
}


Contact* modifyContact(Contact *head,int noContactToModify,
				     char *name, 
				     char *lastName, 
				     char *birthdate, 
				     char *address, 
				     char *phone){	

	Contact *current=head;
	int i=1;
	while (current!=NULL){
		if(i==noContactToModify){
			strcpy(current->name,name);
			strcpy(current->lastName,lastName);
			strcpy(current->birthdate,birthdate);
			strcpy(current->address,address);
			strcpy(current->phone,phone);	 
			return head; 		
		}
		current=current->next;
		i++;
	}
	return head;		
}
	
Contact* getContact(Contact* head, int noContact){
	Contact *current=head;
	int i=1;
	while (current!=NULL){
		if(i==noContact){ return current; }
		current=current->next;
		i++;
	}	
	return NULL;
}

Contact* getLastContact(Contact* head){
	Contact *current=head;
	while (current!=NULL){
		if(current->next==NULL){ return current; }
		current=current->next;
	}	
	return NULL;
}


void saveContact(Contact *contact){
        //creando la linea de objeto en formato que reconozca a librerìa jsmn, se guardarà en una lìnea de un archivo .json, para implementar la persistencia de datos
	int fd;
	char objectJSON[180];
	char temp[75];
	
	strcpy(objectJSON,"{");

	strcat(objectJSON,"\"name\": \"");
	/*esta linea copia el valor, porque strok modifica 
	  internamente el string*/
	strcpy(temp,contact->name);
	strcpy(temp, strtok(temp, "\n"));
	strcat(temp,"\", ");
	strcat(objectJSON,temp);

	strcat(objectJSON,"\"lastName\": \"");
	strcpy(temp,contact->lastName);
        strcpy(temp, strtok(temp, "\n"));
        strcat(temp,"\", ");
        strcat(objectJSON,temp);

	strcat(objectJSON,"\"birthdate\": \"");
	strcpy(temp,contact->birthdate);
        strcpy(temp, strtok(temp, "\n"));
        strcat(temp,"\", ");
        strcat(objectJSON,temp);

	strcat(objectJSON,"\"address\": \"");
	strcpy(temp,contact->address);
        strcpy(temp, strtok(temp, "\n"));
        strcat(temp,"\", ");
        strcat(objectJSON,temp);

	strcat(objectJSON,"\"phone\": \"");
	strcpy(temp,contact->phone);
        strcpy(temp, strtok(temp, "\n"));
        strcat(temp,"\"} \n");
        strcat(objectJSON,temp);
	
	/* comprobar como ingresan los datos al archivo, comentarla una
	   vez terminado el diseño */

	//printf("%s", objectJSON); 
	fd=open("contactos.json", O_CREAT|O_RDWR|O_APPEND);
	write(fd, objectJSON, strlen(objectJSON));
	close(fd);

}
