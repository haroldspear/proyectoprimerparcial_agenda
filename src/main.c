#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <appointment.h> //#include contact esta en appoinment.h

/* Punteros que apuntan al primer elemento de las listas
   enlazadas que almacenan las estructuras Contact y Appointment.
   Estos punteros seran enviados a funciones siempre que se desee
   crear, modificar o borrar un elemento.*/
Contact* headContact=NULL;	
Appointment* headAppointment=NULL;

/* Variables que almacenaran temporalmente valores ingresados por 
   el usuario, para luego crearlos debidamente en las listas
   que los contienen(lista de contactos y de citas). */
Contact contactTemp;
Appointment appointmentTemp;

/* Indices que serviran para buscar un contacto o una cita en las
   listas. */
int noContactToMeet;
int noContactToModify;
int noAppointmentToModify;

int dateIsValid(char date[]);
int optionCFAIsValid(int o);
/* 
	las siguientes funciones tienen como objetivo darle mayor 
	legibilidad al flujo de operaciones en el main y solo son 
	usadas alli, por esa razon las variables de arriba se han 
	definido fuera del main. Porque estas funciones operan con 
	algunas de estas variables.
*/

/*Obtiene nombre, apellido, fecha de nacimiento, direccion y telefono*/
void getContactDataFromUserInput(){
	printf("\nA continuacion ingrese los datos...\n");
	printf("Ingrese un nombre: ");
	fgets(contactTemp.name,20,stdin);
	printf("Ingrese un appellido: ");	
	fgets(contactTemp.lastName,20,stdin);
	do{
		printf("Ingrese nacimiento formato 'dd/mm/yy': ");		
		fgets(contactTemp.birthdate,15,stdin);
		if(!dateIsValid(contactTemp.birthdate)){
			printf("Por favor, asegurese de ingresar valores correctos y ");
			printf("siga el forma formato especificado.\n");
		}
	}while(!dateIsValid(contactTemp.birthdate));
	printf("Ingrese direccion: ");	
	fgets(contactTemp.address,50,stdin);
	printf("Ingrese un numero de telefono: ");	
	fgets(contactTemp.phone,15,stdin);
}

/* obtiene lugar, hora inicio, hora fin y fecha */
void getAppointmentDataFromUserInput(){
	printf("\nA continuacion ingrese los datos...\n");
	printf("Ingrese el lugar: ");
	fgets(appointmentTemp.place,50,stdin);
	printf("Ingrese la hora inicio formato 'hh:mm' : ");	
	fgets(appointmentTemp.startHour,10,stdin);
	printf("Ingrese la hora fin formato 'hh:mm' : ");
	fgets(appointmentTemp.endHour,10,stdin);
	do{		
		printf("Ingrese la fecha formato 'dd/mm/yy': ");	
		fgets(appointmentTemp.appointmentDate,10,stdin);
		if(!dateIsValid(appointmentTemp.appointmentDate)){
			printf("Por favor, asegurese de ingresar valores correctos y ");
			printf("siga el forma formato especificado.\n");		
		}
	}while(!dateIsValid(appointmentTemp.appointmentDate));
	
}

/* los datos ingresados por el usuario que fueron 
   almacenados en la estructura contactTemp, son enviados
   para almacenarlo en la lista de contactos, mediante esta 
   funcion */ 	
void createContactInMain(){
	/* retorno el head porque los cambios hechos en la funcion 
	   son en variable local */
	headContact=createContact(headContact,contactTemp.name, 									      contactTemp.lastName,
									      contactTemp.birthdate,
										  contactTemp.address,
										  contactTemp.phone);
	printf("El contacto fue creado!\n");
}
/* los datos ingresados por el usuario que fueron 
   almacenados en la estructura contactTemp, son enviados
   para modificar la lista de contactos, mediante esta 
   funcion y un indice(noContactToModify)*/ 
void modifyContactInMain(){
	headContact=modifyContact(headContact,
							  noContactToModify,
						      contactTemp.name,
							  contactTemp.lastName,
							  contactTemp.birthdate,
							  contactTemp.address,
							  contactTemp.phone);
	printf("El contacto fue modificado!\n");
}
/* los datos ingresados por el usuario que fueron 
   almacenados en la estructura appointmentTemp, son enviados
   para almacenarlo en la lista de citas, mediante esta 
   funcion. Aqui toma un contacto existente */ 
void createAppointmentInMain_existingContact(){
	headAppointment=createAppointment(headAppointment,
									  appointmentTemp.place,
									  appointmentTemp.startHour,
									  appointmentTemp.endHour,	
									  appointmentTemp.appointmentDate,
									  getContact(headContact,noContactToMeet));	
	printf("La cita fue creada!\n");
}
/* los datos ingresados por el usuario que fueron 
   almacenados en la estructura appointmentTemp, son enviados
   para almacenarlo en la lista de citas, mediante esta 
   funcion. Aqui toma un nuevo contacto */ 
void createAppointmentInMain_newContact(){
	headAppointment=createAppointment(headAppointment,
									  appointmentTemp.place,
									  appointmentTemp.startHour,
									  appointmentTemp.endHour,	
									  appointmentTemp.appointmentDate,
									  getLastContact(headContact));	
	printf("La cita fue creada!\n");
}

/* los datos ingresados por el usuario que fueron 
   almacenados en la estructura appointmentTemp, son enviados
   para modificar una cita en la lista de citas, mediante esta 
   funcion con un indice(noAppointmentToModify). Aqui toma un 
   contacto existente */ 
void modifyAppointmentInMain_existingContact(){
	headAppointment=modifyAppointment(headAppointment,
									  noAppointmentToModify,
									  appointmentTemp.place,
									  appointmentTemp.startHour,
									  appointmentTemp.endHour,	
									  appointmentTemp.appointmentDate,
									  getContact(headContact,noContactToMeet));	
	printf("La cita fue modificada!\n");
}
/* los datos ingresados por el usuario que fueron 
   almacenados en la estructura appointmentTemp, son enviados
   para modificar una cita en la lista de citas, mediante esta 
   funcion con un indice(noAppointmentToModify). Aqui toma un 
   nuevo contacto */ 
void modifyAppointmentInMain_newContact(){
	headAppointment=modifyAppointment(headAppointment,
									  noAppointmentToModify,
									  appointmentTemp.place,
									  appointmentTemp.startHour,
									  appointmentTemp.endHour,	
									  appointmentTemp.appointmentDate,
								 	  getLastContact(headContact));
	printf("La cita fue modificada!\n");
}


int main(){
	int option=0;
	int noContactToDelete;
	int noAppointmentToDelete;
	int noAppointmentToShow;	
	int optionContactForAppointment;
	int m;

	//Las siguientes instancias son para testear con valores iniciales 
	/* les puse \n al final de cada una, porque originalmente se crean 
		mediante entrada de usuario	y uso fgets() que pone siempre \n 
		al guardar las entradas. Las funciones de mostrar contactos 
		tienen un formato en base a eso. */
	headContact=createContact(headContact,"Scarlett\n",
										  "Johanson\n",
										  "05/01/92\n",
										  "Malecon 2000\n",
										  "092389765\n");
	headContact=createContact(headContact,"Sofia\n",
										  "Vergara\n",
										  "01/07/89\n",
										  "32 and Bolivia\n",
										  "096439765\n");
	headContact=createContact(headContact,"Barry\n",
										  "Allen\n",
										  "22/04/90\n",
										  "29 and Argentina\n",
										  "092387865\n");
	headContact=createContact(headContact,"Harry\n",
										  "Potter\n",
										  "03/10/90\n",
										  "Entrada de la 8\n",
										  "092123165\n");
	headContact=createContact(headContact,"Wonder\n",
										  "Woman\n",
										  "11/06/17\n",
										  "Sauces 3\n",
										  "089765465\n");
	headAppointment=createAppointment(headAppointment,"Entre rios\n",
												  	  "13:30\n",
													  "15:30\n",
													  "29/11/16\n",
											 getContact(headContact,1));
	headAppointment=createAppointment(headAppointment,"Hotel Ramada\n",
												  	  "13:30\n",
													  "17:30\n",
													  "30/06/17\n",
											 getContact(headContact,5));

	printf("Bienvenido a la agenda electronica.\n");
	do{
		printf("\t\t\t\t--------\n");
		printf("\t\t\t\t  Menu \n");
		printf("\t\t\t\t--------\n");
		printf("Opciones: \n");
		printf("\tContactos                      \t\tCitas\n");
		printf("1)Crear un contacto             \t6)Crear una cita\n");
		printf("2)Modificar un contacto         \t7)Modificar una cita\n");
		printf("3)Borrar un contacto            \t8)Borrar un cita\n");
		printf("4)Ver lista contactos           \t9)Ver lista citas en detalle\n");
		printf("5)Ver lista contactos en detalle\t10)Ver citas formato calendario\n");

		printf("11) Salir \n");
	
		printf("Escoja una opcion: ");
		
		/* note: when you make the line below, not just checks if the
			input is an int, but check that matches with all
			the cases in the switch. pretty sweet*/
		if(scanf("%d",&option)!=1){ option=0; }

		getchar();
		switch(option){
			case 1:
				getContactDataFromUserInput();				
				createContactInMain();
				break;
			case 2:
				if(headContact!=NULL){
					showContactList(headContact);
       				printf("Numero del contacto a modificar: ");
       				scanf("%i", &noContactToModify); getchar();
					if(existC(headContact,noContactToModify)){
						getContactDataFromUserInput();
						modifyContactInMain();
					}else{ printf("No existe tal contacto \n"); }
					
				}else{ printf("No existen contactos \n");}
				break;
			case 3:	
				if(headContact!=NULL){
					showContactList(headContact);
       				printf("Numero del contacto a borrar: ");
       				scanf("%i", &noContactToDelete); getchar();
					if(existContactInAppointment(headAppointment,
																			 headContact,
																			 noContactToDelete)){
						printf("Lo sentimos, no puede eliminar ese contacto.\n");
						printf("Tiene una cita programada con el mismo.\n");
						printf("Primero borre sus citas pendientes con ese ");
						printf("contacto, y entonces podra borrarlo.\n");
						
					}else{
						headContact=deleteContact(headContact,noContactToDelete);
					}
					
				}else{ printf("No existen contactos \n"); }
				break;
			case 4:	
				showContactList(headContact);
				break;
			case 5:			
				showContactDetails(headContact);
				break;
			case 6:
				getAppointmentDataFromUserInput();					
				do{
							m=0;
							printf("\n(Si decide no, debera crear un nuevo contacto) \n");
							printf("Desea escojer un contacto existente? (y=1/n=0) :");
							m=scanf("%d",&optionContactForAppointment); getchar();
							if(!optionCFAIsValid(optionContactForAppointment)||m!=1){
								printf("Por favor, asegurese de ingresar valores correctos(y=1/n=0)");
							}			
						}while(!optionCFAIsValid(optionContactForAppointment)||m!=1);
					
				if(optionContactForAppointment==1){
					if(headContact!=NULL){
						showContactList(headContact);
       					printf("Numero de contacto con quien sera la cita: ");
       					scanf("%i", &noContactToMeet); getchar();
						if(existC(headContact,noContactToMeet)){
							createAppointmentInMain_existingContact();
						}else{ printf("No existe tal contacto \n"); }
					}else{ printf("No existen contactos \n");}
				}else{
					getContactDataFromUserInput();					
					createContactInMain();
					createAppointmentInMain_newContact();
				}
				break;
			case 7:
				if(headAppointment!=NULL){
					showAppointmentDetails(headAppointment);
       				printf("Numero de cita a modificar: ");
       				scanf("%i", &noAppointmentToModify); getchar();
					if(existA(headAppointment,noAppointmentToModify)){
						getAppointmentDataFromUserInput();						
						do{
							m=0;
							printf("\n(Si decide no, debera crear un nuevo contacto) \n");
							printf("Desea escojer un contacto existente? (y=1/n=0) :");
							m=scanf("%d",&optionContactForAppointment); getchar();
							if(!optionCFAIsValid(optionContactForAppointment)||m!=1){
								printf("Por favor, asegurese de ingresar valores correctos(y=1/n=0)");
							}			
						}while(!optionCFAIsValid(optionContactForAppointment)||m!=1);

						if(optionContactForAppointment==1){
							if(headContact!=NULL){
								showContactList(headContact);
			   					printf("Numero de contacto con quien sera la cita: ");
			   					scanf("%i", &noContactToMeet); getchar();
								if(existC(headContact,noContactToMeet)){
									modifyAppointmentInMain_existingContact();
								}else{ printf("No existe tal contacto \n"); }
							}else{ printf("No existen contactos \n");}
						}else{
							printf("\nAhora creara un contacto para la cita.\n");
							getContactDataFromUserInput();	
							createContactInMain();
							modifyAppointmentInMain_newContact();
						    }
					}else{ printf("No existe esa cita\n"); }
					
				}else{ printf("No existen citas \n");}
				break;
			case 8:	
				if(headAppointment!=NULL){
					showAppointmentDetails(headAppointment);
       				printf("Numero de cita a borrar: ");
       				scanf("%i", &noAppointmentToDelete); getchar();
					headAppointment=deleteAppointment(headAppointment,
													  noAppointmentToDelete);
				}else{ printf("No existen citas \n"); }
				break;			
			case 9:
				showAppointmentDetails(headAppointment);
				break;
			
			case 10:
				if(headAppointment!=NULL){
					showAppointmentDetails(headAppointment);
       				printf("Numero de cita a mostrar : ");
					
       				scanf("%i", &noAppointmentToShow); getchar();
					if(existA(headAppointment,noAppointmentToShow)){
						showAppointmentCalendar(headAppointment,
												  noAppointmentToShow);
					
					}else{ printf(" No existe tal cita \n"); }
													
				}else{ printf("No existen citas \n"); }				
				break;
			case 11:
					printf("Fue un placer ayudarle.\n");
					break;				
			default:
				printf("\n Por favor ingrese un valor valido\n");
				option=0;		
		}
							
	}while(option!=11);		
}

//esta funcion valida el formato de fecha
int dateIsValid(char date[]){
	//date="dd/mm/yy"
	int day=0, month=0, year=0;
	
	day=day*10+(date[0]-'0');
	day=day*10+(date[1]-'0');

	month=month*10+(date[3]-'0');
	month=month*10+(date[4]-'0');
	
	year=year*10+(date[6]-'0');
	year=year*10+(date[7]-'0');	
	//this line was for debug
	//printf("%d %d %d \n",day,month,year);
	if((strlen(date)>=8) && 
	   (day>0 && day <=31) &&
	   (month>0 && month<=12) &&
	   (year>=0 && year <=99)){
		return 1;		
	}	
	return 0;
}

//option contact for appoinment is valid
int optionCFAIsValid(int o){
		if (o==1 || o==0){
			return 1;
		}
		return 0;
}
