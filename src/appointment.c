#include <stdio.h>
#include <string.h>

#include <appointment.h>
#include <unistd.h>
#include <fcntl.h>

Appointment* createAppointment(Appointment *head,char *place, 
				      		 char *startHour, 
				      		 char *endHour, 
				      		 char *appointmentDate,
						 Contact *contactoCita){
	
	Appointment* a=(Appointment*)malloc(sizeof(Appointment));

	strcpy(a->place,place);
	strcpy(a->startHour,startHour);
	strcpy(a->endHour,endHour);
	strcpy(a->appointmentDate,appointmentDate);
	a->contactoCita=contactoCita;
	a->next=NULL;
	saveAppointment(a);
	if(head!=NULL){
		Appointment *current=head;
		while(current->next!=NULL){current=current->next;}
		current->next=a;	
	}else{
		head=a;
	}
	return head;
}
void showAppointmentDetails(Appointment* head){
	Appointment* current=head;
	int i=1; 	
	if(head!=NULL){
		printf("Lista de citas detallada: \n");
		while(current!=NULL){
			printf("%d>> \n", i);
			printf("    Lugar: %s",current->place);
			printf("    Hora Inicio: %s",current->startHour);	
			printf("    Hora Fin: %s",current->endHour);
			printf("    Fecha: %s",current->appointmentDate);
			printf("    Con: %s",current->contactoCita->name);
			printf("         %s",current->contactoCita->lastName);
			current=current->next;
			i++;
		}
	}else{
		printf("No existen citas pendientes \n");
	}
}
int existA(Appointment* head, int noAppointmentToModify){
	Appointment *current=head;
	int i=1;
	while (current!=NULL){
		if(i==noAppointmentToModify){ return 1; }
		current=current->next;
		i++;
	}	
	return 0;
}

Appointment* modifyAppointment(Appointment* head,int noAppointmentToModify,
						 char *place, 
				      		 char *startHour, 
				      		 char *endHour, 
				      		 char *appointmentDate,
						 Contact *contactoCita){
	Appointment *current=head;
	int i=1;
	while (current!=NULL){
		if(i==noAppointmentToModify){
			strcpy(current->place, place);
			strcpy(current->startHour,startHour);
			strcpy(current->endHour,endHour);
			strcpy(current->appointmentDate,appointmentDate);	 
			current->contactoCita=contactoCita;
			return head; 		
		}
		current=current->next;
		i++;
	}
	return head;
	
}


Appointment* deleteAppointment(Appointment *head,int noAppointmentToDelete){
	Appointment *current=head;
	Appointment *previous=NULL;
	int i=1;

	while (current!=NULL){
		if(i==noAppointmentToDelete){
			if (previous==NULL){
				head = current->next;
				free(current);
				printf("La cita fue borrada!\n");
				return head;
			}else{
				previous->next = current->next;
				free(current);
				printf("La cita fue borrada!\n");
				return head;
			}
		}
		previous=current;
		current=current->next;
		i++;
	}	
	printf("No existe tal cita\n");
	return head;
}

void saveAppointment(Appointment *appointment){
        //creando la linea de objeto en formato que reconozca a librerìa jsmn, se guardarà en una lìnea de un archivo .json, para implementar la persistencia de datos
	int fd;
	char objectJSON[180];
	char temp[75];

	strcpy(objectJSON,"{");

	strcat(objectJSON,"\"place\": \"");
	/*esta linea copia el valor, porque strok modifica 
	  internamente el string*/
	strcpy(temp, appointment->place);
	strcpy(temp, strtok(temp, "\n"));
	strcat(temp,"\", ");
	strcat(objectJSON,temp);

	strcat(objectJSON,"\"startHour\": \"");
	strcpy(temp, appointment->startHour);
	strcpy(temp, strtok(temp, "\n"));
        strcat(temp,"\", ");
        strcat(objectJSON,temp);

	strcat(objectJSON,"\"endHour\": \"");
	strcpy(temp, appointment->endHour);
        strcpy(temp, strtok(temp, "\n"));
        strcat(temp,"\", ");
        strcat(objectJSON,temp);

	strcat(objectJSON,"\"appointmentDate\": \"");
	strcpy(temp, appointment->appointmentDate);
	strcpy(temp, strtok(temp, "\n"));
        strcat(temp,"\", ");
        strcat(objectJSON,temp);

	strcat(objectJSON,"\"contact\": \"");
	strcpy(temp, appointment->contactoCita);
	strcpy(temp, strtok(temp, "\n"));
        strcat(temp,"\"} \n");
        strcat(objectJSON,temp);

	/* comprobar como ingresan los datos al archivo, comentarla una
	   vez terminado el diseño */ 
	//printf("%s", objectJSON);
	fd=open("citas.json", O_CREAT|O_RDWR|O_APPEND);
	write(fd, objectJSON, strlen(objectJSON));
	close(fd);

}

void showAppointmentCalendar(Appointment *head,int noAppointmentToShow){
	Appointment *current=head;
	int month=0,year=0;
	int i=1,k;
	//char tempDate[10];
	//strcpy(tempDate,current->appointmentDate);
	while (current!=NULL){
		if(i==noAppointmentToShow){
			//date="dd/mm/yy"
			for(k=3;k<5;k++){
				month=month*10+(current->appointmentDate[k]-'0');
			}
			for(k=6;k<8;k++){
				year=year*10+(current->appointmentDate[k]-'0');
			}	
			year=2000+year;
			printCalendar(month,year);			
	
			printf("    Lugar: %s",current->place);
			printf("    Hora Inicio: %s",current->startHour);	
			printf("    Hora Fin: %s",current->endHour);
			printf("    Fecha: %s",current->appointmentDate);
			printf("    Con: %s",current->contactoCita->name);
			printf("         %s",current->contactoCita->lastName);
		
		}
		current=current->next;
		i++;
	}
}

int existContactInAppointment(Appointment* headA,Contact* headC, int noC){
	Contact* contact=getContact(headC,noC);
	
	Appointment *current=headA;
	int i=1;
	while (current!=NULL){
		if(current->contactoCita==contact){
			return 1; 		
		}
		current=current->next;
		i++;
	}
	return 0;
	
}
