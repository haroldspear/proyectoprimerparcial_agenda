TODO=./lib/libcontact.a ./lib/libappointment.a ./lib/libcalendario.a contact.o appointment.o calendario.o main.o
CC=gcc
AR=ar
IDIR=./include
CFLAGS=-I$(IDIR)
DEPS=$(IDIR)

all:$(TODO)

./lib/libcontact.a: contact.o
	$(AR) rcs $@ $^

./lib/libappointment.a: appointment.o
	$(AR) rcs $@ $^

./lib/libcalendario.a: calendario.o
	$(AR) rcs $@ $^

contact.o: 
	$(CC) -c ./src/contact.c $(CFLAGS)

appointment.o: 
	$(CC) -c ./src/appointment.c $(CFLAGS)

calendario.o:
	$(CC) -c ./src/calendario.c $(CFLAGS)

main.o:
	$(CC) -c ./src/main.c $(CFLAGS)

static: 
	gcc -static main.o ./lib/libappointment.a ./lib/libcontact.a ./lib/libappointment.a ./lib/libcontact.a ./lib/libcalendario.a -o agenda
clean:
	rm *.o agenda
