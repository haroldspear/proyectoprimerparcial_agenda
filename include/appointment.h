#include <stdlib.h>

#include <contact.h>
typedef struct Appointment{
	char place[50];
	char startHour[10];//format "hh:mm"
	char endHour[10];
	char appointmentDate[15];//format dd/mm/yyyy
	struct Contact *contactoCita;
	struct Appointment *next;
}Appointment;


Appointment* createAppointment(Appointment *head,char *place, 
				      		 char *startHour, 
				      		 char *endHour, 
				      		 char *appointmentDate,
						 Contact *contactoCita);

Appointment* modifyAppointment(Appointment* head,int noAppointmentToModify,
						 char *place, 
				      		 char *startHour, 
				      		 char *endHour, 
				      		 char *appointmentDate,
						 Contact *contactoCita);

Appointment* deleteAppointment(Appointment *head,int noAppointmentToDelete);

void showAppointmentDetails(Appointment* head);
int existA(Appointment* head, int noAppointmentToModify);

void saveAppointment(Appointment *appointment);
void showAppointmentCalendar(Appointment *head,int noAppointmentToShow);
int existContactInAppointment(Appointment* headA,Contact* headC, int noC);

