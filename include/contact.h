#include <stdlib.h>

typedef struct Contact{
	char name[20];
	char lastName[20];
	char birthdate[15];//format dd/mm/yy
	char address[50];
	char phone[15];
	struct Contact *next;
}Contact;//when we create the appointment we linked with a conctact

void showContactList(Contact*);
void showContactDetails(Contact*);

Contact* createContact(Contact *head,char *name, 
				     char *lastName, 
				     char *birthdate, 
				     char *address, 
				     char *phone);

Contact* modifyContact(Contact *head,int noContactToModify,
				     char *name, 
				     char *lastName, 
				     char *birthdate, 
				     char *address, 
				     char *phone);

Contact* deleteContact(Contact *head,int noContactToDelete);
int existC(Contact* head, int noContactToModify);
Contact* getContact(Contact* head, int noContact);
Contact* getLastContact(Contact* head);

void saveContact(Contact*);


