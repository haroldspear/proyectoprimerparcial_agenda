Agenda Electronica en Lenguaje C.
Proyecto del Primer Parcial de Programacion de Sistemas 2017 1er Termino

Integrantes:
	Alejandro Mena
	Harold Aragon

Funcionalidad: 
	El programa consta de un menu de 10 opciones y una opcion para salir.
	5 opciones para contactos y 5 para citas.

	1)Crear un contacto 
		Crea un contacto pidiendole al usuario ingresar Nombre,Apellido,
		fecha de cumpleaños, direccion y numero de telefono.
			
	2)Modificar un contacto 
		Muestra la lista de contactos existentes ennumerados y se le pide al 
		usuario ingresar el numero del contacto a modificar. Si no hay contactos
		o si el indice no corresponde a los contactos mostrados, regresa al menu.

		Modifica un contacto pidiendole al usuario ingresar Nombre,Apellido,
		fecha de cumpleaños, direccion y numero de telefono.
		
	3)Borrar un contacto 
		Muestra la lista de contactos existentes ennumerados y se le pide al 
		usuario ingresar el numero del contacto a borrar. Si no hay contactos
		o si el indice no corresponde a los contactos mostrados, regresa al menu.

		Si el contacto esta ligado a una cita existente, no se podra borrar.
		Se tendra que borrar primero la cita y luego el contacto asociado a esa 
		cita.
	4)Ver lista contactos
		Muestra una lista de los contactos existentes con su nombre y appellido.
	5)Ver lista contactos en detalle
		Muestra una lista de los contactos existentes con todos sus campos


	Citas, las funciones son muy similares a las de contacto.

	6)Crear cita
		Crea un cita pidiendole al usuario ingresar Lugar, hora inicio y
		hora fin en formato 'hh:mm', la fecha en formato 'dd/mm/yy' y el contacto
		con quien sera la cita.Para esto el usuario tendra 2 opciones:

			Si decide escojer un contacto existente, se vera la lista de contactos
			y el usuario escoje mediante un indice, al contacto con quien se citara.

			De lo contrario tendra que crear un nuevo contacto, con el mismo 
			procedimiento que se crea un contacto desde el inicio.
	
	7)Modificar una cita
		Se muestra la lista de citas existentes ennumerados y se le pide al 
		usuario ingresar el numero de la cita a modificar. Si no hay citas
		o si el indice no corresponde a las citas mostradas, regresa al menu.
		
		Modifica un cita pidiendole al usuario ingresar Lugar, hora inicio y 
		hora fin en formato 'hh:mm', la fecha en formato 'dd/mm/yy' y el 
		contacto con quien sera la cita.Para esto el usuario tendra 2 opciones:

			Si decide escojer un contacto existente, se vera la lista de contactos
			y el usuario escoje mediante un indice, al contacto con quien se citara.

			De lo contrario tendra que crear un nuevo contacto, con el mismo 
			procedimiento que se crea un contacto desde el inicio.

	8)Borrar una cita
		Muestra la lista de citas existentes ennumeradas y se le pide al 
		usuario ingresar el numero de la cita a borrar. Si no hay citas
		o si el indice no corresponde a las citas mostradas, regresa al menu.
		
		Al contrario que con los contactos, una cita si puede ser borrada sin
		importar al contacto con el que esta asociado. El contacto permanece
		almacenado una vez eliminada la cita.
	9)Ver lista citas en detalle
		Muestra las citas existentes con todos sus campos.
	10)Ver citas calendario mensual
		Se muestra todas las citas existentes con indices y se le pide al usuario
		que escoja, cual desea ver en formato calendario.
		El usuario escoje un indice y se muestra un calendario acorde al año y mes
		de la cita escojida por el usuario.

	Validaciones:
		Existen validaciones para las opciones del menu.
		En la entrada de datos existen validaciones solo para el formato
		de fecha 'dd/mm/yy'. Para los demas datos, el usuario podria ingresar
		cualquier valor y se almacenaran como correctos.
		Existe validacion en la opcion de escoger usar un contacto existente
		para la cita o crear uno nuevo.
		Existe validacion al momento de eliminar un contacto, solo si no tiene
		una cita asociada.

		Todas las validaciones que contribuyen al flujo normal de trabajo del 		
		programa han sido implementadas, pero se nombraron las mas importantes.

	Notas:
		Se han cargado en el main 5 ejemplos de contactos y 2 ejemplos de citas,
		para probar directamente las opciones de mostrar lista de contacto, 
		borrar contactos, mostrar lista de contactos detallada, mostrar citas,
		mostrar citas formato calendario y borrar citas.

		El programa solo implementa persistencia con la libreria jsmn, con 		
		escritura. La misma que funcionaba hasta antes de juntar todos los 
		ficheros y crear las librerias. Se puede chequear esto en las funciones 	
		saveContact() y saveAppointment() de los ficheros contact.c y 		
		appointment.c respectivamente, ubicados en la carpeta src.
                
                Para compilar se lo hace con make y luego make static.